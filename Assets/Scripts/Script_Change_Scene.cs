﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_Change_Scene : MonoBehaviour
{
    public string loadedScene;

    public void ChangeScene()
    {
        // This will load the selected scene
        SceneManager.LoadScene(loadedScene);
    }

}
