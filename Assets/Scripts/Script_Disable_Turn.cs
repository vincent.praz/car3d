﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Disable_Turn : MonoBehaviour
{
    Script_Turn scriptTurn;
    bool turningAllowed = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Build the model3D string name
        string model3D = PlayerPrefs.GetString("Brand") + "_" + PlayerPrefs.GetString("Model") + "_" + PlayerPrefs.GetString("Year");
        // Retrieve the recently created 3dmodel object
        GameObject car = GameObject.Find("Car_Model");

        // Retrieve script turn of the model
        scriptTurn = car.GetComponent<Script_Turn>();
    }

    /* Disables the Turning of the Car */
    public void NoTurningAllowed()
    {
        turningAllowed = false;
        scriptTurn.NoTurningAllowed();
    }

    /* Enables the Turning of the Car */
    public void TurningAllowed()
    {
        turningAllowed = true;
        scriptTurn.TurningAllowed();
    }
}
