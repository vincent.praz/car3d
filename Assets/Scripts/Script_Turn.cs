﻿using UnityEngine;
using UnityEngine.UI;

public class Script_Turn : MonoBehaviour
{

    protected Vector3 posLastFame;
    float minFov  = 15f;
    float maxFov = 50f;
    public float speed = 1f;
    public Camera selectedCamera; //pass at the script
    public float MINSCALE = 2.0F;
    public float MAXSCALE = 5.0F;
    public float minPinchSpeed = 5.0F;
    public float varianceInDistances = 5.0F;
    private float touchDelta = 0.0F;
    private Vector2 prevDist = new Vector2(0, 0);
    private Vector2 curDist = new Vector2(0, 0);
    private float speedTouch0 = 0.0F;
    private float speedTouch1 = 0.0F;


    Touch touch;


    private bool turningAllowed = true;
    
    /* Disable the Turning of the Car */
    public void NoTurningAllowed()
    {
        turningAllowed = false;
    }

    /* Enables the Turning of the Car */
    public void TurningAllowed()
    {
        turningAllowed = true;
    }


    // Update is called once per frame
    void Update()
    {

        selectedCamera = Camera.main;

        // If only one touch, turn around car
        if (Input.touchCount == 1 && turningAllowed) {
            if (Input.GetMouseButtonDown(0))
            {
                posLastFame = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                var delta = Input.mousePosition - posLastFame;
                posLastFame = Input.mousePosition;

                // var axis = Quaternion.AngleAxis(-90f, Vector3.forward) * delta;

                transform.Rotate(0f, delta.x * -0.1f, 0f, Space.World);
                transform.Rotate(0f, 0f, delta.y * 0.2f, Space.Self);

            }
        }


        // Pinch zoom
        if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved) 
        {
            curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; //current distance between finger touches
            prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta positions
            touchDelta = curDist.magnitude - prevDist.magnitude;
            speedTouch0 = Input.GetTouch(0).deltaPosition.magnitude / Input.GetTouch(0).deltaTime;
            speedTouch1 = Input.GetTouch(1).deltaPosition.magnitude / Input.GetTouch(1).deltaTime;


            // Zoom
            if ((touchDelta + varianceInDistances <= 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
            {
                selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView + speed, minFov, maxFov);
            }


            // Dezoom
            if ((touchDelta + varianceInDistances > 1) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
            {
                selectedCamera.fieldOfView = Mathf.Clamp(selectedCamera.fieldOfView - speed, minFov, maxFov);
            }
        }


        // Move Car
        if (Input.touchCount == 3)
        {
            touch = Input.GetTouch(2);
            if(touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(
                    transform.position.x - touch.deltaPosition.x * 0.001f,
                    transform.position.y + touch.deltaPosition.y * 0.001f,
                    transform.position.z);
            }
        }
    }
}
