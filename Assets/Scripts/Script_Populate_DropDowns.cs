﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

// This script is used to populate the brand dropdown list
public class Script_Populate_DropDowns : MonoBehaviour
{
    //This is the Dropdown_Brand
    Dropdown Dropdown_Brand;
    Dropdown Dropdown_Model;
    Dropdown Dropdown_Year;
    Button modelise;

    // The selectedBrand text
    string selectedBrand;


    struct Model
    {
        public string modelName;
        public List<string> years;
    }

    struct Brand
    {
        public string brandName;
        public List<Model> models;
    }

    struct Car
    {
        public List<Brand> brands;
    }

    Car car;

    void Start()
    {
        modelise = GameObject.Find("Button_Modelise").GetComponent<Button>();
        //Fetch the Dropdown GameObject the script is attached to
        Dropdown_Brand = GetComponent<Dropdown>();
        //Clear the old options of the Dropdown menu
        Dropdown_Brand.ClearOptions();

        // Retrieve other dropdowns
        Dropdown_Model = GameObject.Find("Dropdown_Model").GetComponent<Dropdown>();
        Dropdown_Year = GameObject.Find("Dropdown_Year").GetComponent<Dropdown>();


        Text defaultText = Dropdown_Brand.GetComponentInChildren<Text>();
        defaultText.text = "Brand";

        
        Text defaultTextYear = Dropdown_Year.GetComponentInChildren<Text>();
        defaultTextYear.text = "Year";
        //Add the default option to the dropdown Because its bugged
        Dropdown.OptionData defaultOptionYear = new Dropdown.OptionData();
        defaultOptionYear.text = "Year";
        Dropdown_Year.options.Add(defaultOptionYear);



        Text defaultTextModel = Dropdown_Model.GetComponentInChildren<Text>();
        defaultTextModel.text = "Model";

        


        //Add the default option to the dropdown
        Dropdown.OptionData defaultOption = new Dropdown.OptionData();
        defaultOption.text = "Brand";
        Dropdown_Brand.options.Add(defaultOption);



        // Retrieve all Car names from the ressources
        car = new Car();
        foreach (var t in Resources.LoadAll("Cars"))
        {
            string[] infoCar = t.name.Split('_');
            string brand = infoCar[0];
            string model = infoCar[1];
            string year = infoCar[2];

           
            // If there are no existing brand, creates a new brand with model + year
            if(car.brands == null || !(car.brands.Exists(x=> x.brandName == brand))){
                Debug.Log("Creation brand");

                Brand brandStruct = new Brand();
                brandStruct.brandName = brand;

                Model modelStruct = new Model();
                modelStruct.modelName = model;
                modelStruct.years = new List<string>();
                modelStruct.years.Add(year);

                brandStruct.models = new List<Model>();
                brandStruct.models.Add(modelStruct);

                // Instantiate the list if it isnt
                if(car.brands == null)
                {
                    car.brands = new List<Brand>();
                }

                car.brands.Add(brandStruct);
            }
            else{
                // Search for the currently looped on brand
                Brand brandStruct = car.brands.Find(x => x.brandName == brand);
                //  if the model doesn't exists create one and add it to the models list of the brand
                if(!brandStruct.models.Exists(x => x.modelName == model))
                {
                    Model modelStruct = new Model();
                    modelStruct.modelName = model;
                    modelStruct.years = new List<string>();
                    modelStruct.years.Add(year);
                    brandStruct.models.Add(modelStruct);
                }
                else
                {
                    // If the year doesn't exists, adds the years list of the model of the brand
                    Model modelStruct = brandStruct.models.Find(x => x.modelName == model);
                    modelStruct.years.Add(year);
                }
            }
        }

        // Retrieve all brand names
        foreach (Brand brand in car.brands)
        {
            // Get the brand name of the brand looped on
            string brandName = brand.brandName;

            //Create a new option for the Dropdown menu
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = brandName;

            //Add each entry to the Dropdown
            Dropdown_Brand.options.Add(option);
        }



        //Add listener for when the value of the Dropdown changes, to take action
        Dropdown_Brand.onValueChanged.AddListener(delegate
        {
            DropdownValueChanged(Dropdown_Brand);
        });



        //Add listener for when the value of the Dropdown changes, to take action
        Dropdown_Model.onValueChanged.AddListener(delegate
        {
            DropdownModelValueChanged(Dropdown_Model);
        });



        //Add listener for when the value of the Dropdown changes, to take action
        Dropdown_Year.onValueChanged.AddListener(delegate
        {
            DropdownYearValueChanged(Dropdown_Year);
        });
    }

    // Loads the options in the Dropdown for Models
    void DropdownValueChanged(Dropdown change)
    {
        selectedBrand = Dropdown_Brand.options[change.value].text;
        
        // Avoid searching for the option category if the default value is selected
        if (selectedBrand == "Brand") { return; };


        // Save the selected brand in the playerprefs
        PlayerPrefs.SetString("Brand", selectedBrand);


        // Retrieve the dropdown for the car model choice
        Dropdown Dropdown_Model = GameObject.Find("Dropdown_Model").GetComponent<Dropdown>();
        Dropdown_Model.ClearOptions();

        Text defaultText = Dropdown_Model.GetComponentInChildren<Text>();
        defaultText.text = "Model";

        Text defaultTextyr = Dropdown_Year.GetComponentInChildren<Text>();
        defaultTextyr.text = "Year";

        //Add the default option to the dropdown
        Dropdown.OptionData defaultOption = new Dropdown.OptionData();
        defaultOption.text = "Model";
        Dropdown_Model.options.Add(defaultOption);



        // Retrieve all brands
        Brand brand = car.brands.Find(x => x.brandName == selectedBrand);
        foreach (Model model in brand.models)
        {
            // Get the model name of the model looped on
            string modelName = model.modelName;

            //Create a new option for the Dropdown menu
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = modelName;

            //Add each entry to the Dropdown
            Dropdown_Model.options.Add(option);
        }

        // Make the dropdown interactable
        Dropdown_Model.interactable = true;
        Dropdown_Year.interactable = false;
        modelise.interactable = false;
    }



    // Loads the options in the Dropdown for Years
    void DropdownModelValueChanged(Dropdown change)
    {
        // Retrieve the dropdown for the car model choice
        Dropdown Dropdown_Model = GameObject.Find("Dropdown_Model").GetComponent<Dropdown>();

        // The selected Model text
        string selectedModel = Dropdown_Model.options[change.value].text;

        // Avoid searching for the option category if the default value is selected
        if (selectedModel == "Model") { return; };

        // Save the selected model in the playerprefs
        PlayerPrefs.SetString("Model", selectedModel);


        // Retrieve the dropdown for the car year choice
        Dropdown_Year.ClearOptions();

        Text defaultText = Dropdown_Year.GetComponentInChildren<Text>();
        defaultText.text = "Year";


        //Add the default option to the dropdown
        Dropdown.OptionData defaultOption = new Dropdown.OptionData();
        defaultOption.text = "Year";
        Dropdown_Year.options.Add(defaultOption);



        // Retrieve all years
        Brand brand = car.brands.Find(x => x.brandName == selectedBrand);
        Model model = brand.models.Find(x => x.modelName == selectedModel);
        foreach (string year in model.years)
        {
            //Create a new option for the Dropdown menu
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = year;

            //Add each entry to the Dropdown
            Dropdown_Year.options.Add(option);
        }

        // Make the dropdown interactable
        Dropdown_Year.interactable = true;
        modelise.interactable = false;
    }

    void DropdownYearValueChanged(Dropdown change)
    {
        Dropdown Dropdown_Year = GameObject.Find("Dropdown_Year").GetComponent<Dropdown>();
        // The selected year text
        string selectedYear = Dropdown_Year.options[change.value].text;

        // Avoid searching for the option category if the default value is selected
        if (selectedYear == "Year") { return; };

        // Save the selected model in the playerprefs
        PlayerPrefs.SetString("Year", selectedYear);

        modelise.interactable = true;
    }
}