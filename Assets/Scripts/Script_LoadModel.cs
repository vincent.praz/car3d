﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_LoadModel : MonoBehaviour
{
    // Layers Gameobjects
    bool selected = false;
    GameObject lastPieceClicked;
    Button lastButtonSelected;
    GameObject layer0;
    GameObject layer1;
    GameObject layer2;
    GameObject layer3;

    GameObject car_model;

    public Slider mySlider;
    public Vector3 StartRotation;
    public GameObject SpawnedCar;
    public List<string> AllPieces;
    public List<string> FilterPieces;

    // TO REMOVE
    public List<GameObject> AllGameObj;

    // Start is called before the first frame update
    void Start()
    {
        StartRotation = new Vector3(0, 0, 0);
        mySlider = GameObject.Find("Slider").GetComponent<Slider>();
        // Build the path string
        string path = "Cars/" + PlayerPrefs.GetString("Brand") + "/" + PlayerPrefs.GetString("Model") + "/" + PlayerPrefs.GetString("Year") + "/";

        // Build the model3D string name
        string model3D = PlayerPrefs.GetString("Brand") + "_" + PlayerPrefs.GetString("Model") + "_" + PlayerPrefs.GetString("Year");

        car_model = GameObject.Find("Car_Model");
        car_model.transform.position = new Vector3(0, 0.45f, 0);
        // Loads the 3D model
        SpawnedCar = Instantiate(Resources.Load(path + model3D), new Vector3(0, -0.65f, 0), Quaternion.identity) as GameObject;
        SpawnedCar.transform.SetParent(car_model.transform, false);

        // Retrieve the recently created 3dmodel object
        GameObject car = GameObject.Find(model3D + "(Clone)");

        // Add sidebar piece list
        foreach (Transform layer in car.transform)
        {
            foreach (Transform childchild in layer)
            {
                AllPieces.Add(childchild.name);
            }
        }
        FilterPieces = AllPieces;
        // Populate SideBar
        renderScroll();

        // Retrieve layers gameobjects
        layer0 = GameObject.Find("Layer0");
        layer1 = GameObject.Find("Layer1");
        layer2 = GameObject.Find("Layer2");
        layer3 = GameObject.Find("Layer3");

    }
    void ZoomAndHighlight(string pieceString, Button clickedButton)
    {

        SpawnedCar.transform.position = StartRotation;

        // Reactivate layers GameObjects
        layer0.SetActive(true);
        layer1.SetActive(true);
        layer2.SetActive(true);
        layer3.SetActive(true);

        // Retrieve piece Gameobject
        GameObject piece = GameObject.Find(pieceString);



        car_model.transform.position = StartRotation;

        // Select / Unselect Button
        if (lastButtonSelected == clickedButton)
        {
            // Unselect
            GameObject myEventSystem2 = GameObject.Find("EventSystem");
            myEventSystem2.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
            lastButtonSelected = null;

            // Reset camera
            SpawnedCar.transform.position = StartRotation;
        }
        else
        {
            // Select
            lastButtonSelected = clickedButton;


            // Set position to rotate around the selected piece
            SpawnedCar.transform.position = StartRotation;
            SpawnedCar.transform.position = -piece.transform.position;
        }

        // Retrieve Gameobject's parent (Layer)
        GameObject layerGameObject = piece.transform.parent.gameObject;
        // Retrieve the layer name
        string layer = layerGameObject.name;

        // Switch case on layer name
        switch (layer)
        {
            case "Layer0":
                // NOTHING hidden
                mySlider.value = 0;
                break;
            case "Layer1":
                // Hide layer 0
                layer0.SetActive(false);
                mySlider.value = 1;
                break;
            case "Layer2":
                // Hide layer 0 - 1
                layer0.SetActive(false);
                layer1.SetActive(false);
                mySlider.value = 2;
                break;
            case "Layer3":
                // Hide layer 0 - 1 - 2
                layer0.SetActive(false);
                layer1.SetActive(false);
                layer2.SetActive(false);
                mySlider.value = 3;
                break;
            default:
                Debug.Log("ERROR !");
                break;
        }

        // Disable all other outlines + unselect
        if (lastPieceClicked)
        {
            RecursiveChildrenOutlining(lastPieceClicked, false);
        }

        // Outline our piece + childs of the piece
        RecursiveChildrenOutlining(piece, true);

        lastPieceClicked = piece;
    }

    /* Recursively adds/removes outlines to the piece gameobject in parameters and its childrens if they have a MeshRenderer */
    void RecursiveChildrenOutlining(GameObject piece, bool add)
    {
        // Check if the piece has a MeshRenderer && is in add outline mode
        if (piece.GetComponent<MeshRenderer>() && add == true)
        {
            // Add outline to the piece
            addOutline(piece);
        }
        else if (!add)
        {
            // Remove outline of the piece
            Destroy(piece.GetComponent<cakeslice.Script_Outline>());
        }

        // Check if the piece has childs
        if (piece.transform.childCount > 0)
        {
            // Retrieve number of childs
            int numberOfChilds = piece.transform.childCount;

            // Loops on all the piece childs
            for (int i = 0; i < numberOfChilds; ++i)
            {
                // Recursion on the childs to add/remove outline to all children and of childrens etc
                RecursiveChildrenOutlining(piece.transform.GetChild(i).gameObject, add);

            }
        }
    }

    /* Adds an outline to the gameobject parameter */
    void addOutline(GameObject gameObject)
    {
        if (!gameObject.GetComponent<cakeslice.Script_Outline>())
        {
            cakeslice.Script_Outline outline = gameObject.AddComponent<cakeslice.Script_Outline>();
            outline.color = 0;
        }
    }

    // Manage Layers
    public void MainVolumeControl(float layer)
    {
        // Reactivate layers GameObjects
        layer0.SetActive(true);
        layer1.SetActive(true);
        layer2.SetActive(true);
        layer3.SetActive(true);

        // Switch case on layer name
        switch (layer)
        {
            case 0:
                // NOTHING hidden
                break;
            case 1:
                // Hide layer 0
                layer0.SetActive(false);
                break;
            case 2:
                // Hide layer 0 - 1
                layer0.SetActive(false);
                layer1.SetActive(false);
                break;
            case 3:
                // Hide layer 0 - 1 - 2
                layer0.SetActive(false);
                layer1.SetActive(false);
                layer2.SetActive(false);
                break;
            default:
                Debug.Log("ERROR !");
                break;
        }
    }
    public void SearchBar(string input)
    {
        // Remove all pieces from side scroll list
        foreach (string piece in FilterPieces)
        {
            Destroy(GameObject.Find("Button_" + piece));
        }
        //filtrer mon tab
        input = input.ToLower();
        FilterPieces = AllPieces.FindAll(e => e.ToLower().Contains(input));

        //rerender scroll list
        renderScroll();
    }
    public void renderScroll()
    {
        Transform SpawnPoint = GameObject.Find("SpawnPoint").transform;
        GameObject item = Resources.Load("Item") as GameObject;
        RectTransform content = GameObject.Find("Content").GetComponent<RectTransform>();
        Font m_Font = Resources.Load("Exo2-Regular") as Font;
        Color32 c_fond = new Color32(29, 28, 28, 255);
        Color32 c_txt = new Color32(215, 213, 216, 255);
        float spawnY = 0;
        foreach (string childchild in FilterPieces)
        {
            //newSpawn Position
            Vector3 pos = new Vector3(0, -spawnY, SpawnPoint.position.z);
            //instantiate item
            GameObject SpawnedItem = Instantiate(item, pos, SpawnPoint.rotation);
            SpawnedItem.name = "Button_" + childchild;
            //setParent
            SpawnedItem.transform.SetParent(SpawnPoint, false);
            //get ItemDetails Component
            ItemDetails itemDetails = SpawnedItem.GetComponent<ItemDetails>();
            //set name
            string pieceString = childchild;
            itemDetails.GetComponent<Image>().color = c_fond;
            itemDetails.text.text = pieceString;
            itemDetails.text.fontSize = 20;
            itemDetails.text.font = m_Font;
            itemDetails.text.color = c_txt;
            itemDetails.text.rectTransform.anchorMin = new Vector2(0, 0.5f);
            itemDetails.text.rectTransform.anchorMax = new Vector2(1, 0.5f);
            itemDetails.text.rectTransform.offsetMin = new Vector2(20, itemDetails.text.rectTransform.offsetMin.y);
            itemDetails.text.rectTransform.offsetMax = new Vector2(0, itemDetails.text.rectTransform.offsetMax.y);
            Button clickedButton = SpawnedItem.GetComponent<Button>();
            clickedButton.onClick.AddListener(delegate { ZoomAndHighlight(pieceString, clickedButton); });

            // 100 width of item
            spawnY += 101;
        }
        //setContent Holder Height;
        content.sizeDelta = new Vector2(0, FilterPieces.Count * 101);
    }
}
