﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace cakeslice
{
    public class OutlineAnimation : MonoBehaviour
    {
        bool pingPong = false;
        //double alpha = 0f;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Color c = GetComponent<OutlineEffect>().lineColor0;

            // Old code
            /*
            if (pingPong)
            {
                c.a += Time.deltaTime;

                if (c.a >= 1)
                    pingPong = false;
            }
            else
            {
                c.a -= Time.deltaTime;

                if (c.a <= 0)
                    pingPong = true;
            }*/

            /*
            if(alpha != 1f)
            {
                c.a += 0.1f;
            }
            else
            {
                c.a = 0f;
            }*/

            // Custom code for Breathing slower animation
            if (pingPong)
            {
                if (c.a < 1)
                {
                    c.a += 0.001f;
                }
                else
                {
                    pingPong = false;
               }
            }
            else
            {
                if (c.a > 0.5f)
                {
                    c.a -= 0.001f;
                }
                else
                {
                    pingPong = true;
                }
            }

            c.a = Mathf.Clamp01(c.a);
            GetComponent<OutlineEffect>().lineColor0 = c;
            GetComponent<OutlineEffect>().UpdateMaterialsPublicProperties();
        }
    }
}