Scenes :
Menu_CarSelect : Main car selection menu with dropdowns
CarView : Scene where the 3D model will be loaded and displayed dynamically

3D Models :
Assets/Cars/RealisticMobileCars - Pro 3D Models : Toyota AE86 3D model => Prefab edited in Assets/Resources/Cars/Toyota/AE86/1984
Assets/Cars/nissan-240z-rocket-bunny : Nissan 240z 3D Models => Prefab edited in Assets/Resources/Cars/Nissan/240Z/1970
Assets/Cars/Best Sports CARS - Pro 3D Models : Toyota GT86 3D model => Unused for now
Assets/automatic-wristwatch-automatic-watch : Watch 3D Model => Prefab edited in Assets/Resources/Cars/Watch/Watch/Watch
Assets/traditional-car-chassis : Chassis 3D Model => Edited and added in our other cars models
Assets/Engine_OBJ : Engine 3D Model => Edited and added in our other cars models

Scripts :
Script_Populate_Dropdowns : Done by ourselves
Script_Turn : Done by ourselves
Script_Outline : Took it online and modified
OutlineEffect : Took it online and modified
OutlineAnimation : Took it online and modified
Script_LoadModel : Done by ourselves
Script_Disable_Turn : Done by ourselves
Script_Change_Scene : Done by ourselves
ListCreator : Took it online and modified
LinkedSet : Took it online and modified
ItemDetails : Took it online and modified


Attention : You have to have blender installed to be able to build this application, otherwise, some 3D models may not be loaded properly